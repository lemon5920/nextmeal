import Vue from 'vue'
import Router from 'vue-router'
import Restaurants from '../components/Restaurants'
import InsertRestaurant from '../components/InsertRestaurant'
import RestaurantDetail from '../components/RestaurantDetail'
import HelloWorld from '../components/HelloWorld'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: { name: 'Restaurants' }
    },
    {
      path: '/Restaurants',
      name: 'Restaurants',
      component: Restaurants
    },
    {
      path: '/InsertRestaurant',
      name: 'InsertRestaurant',
      component: InsertRestaurant
    },
    {
      path: '/RestaurantDetail/:id',
      name: 'RestaurantDetail',
      component: RestaurantDetail
    },
    {
      path: '/HelloWorld',
      name: 'HelloWorld',
      component: HelloWorld
    },
  ]
})
