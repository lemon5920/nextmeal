import firebase from 'firebase/app'
import 'firebase/auth';

const moduleOfUser = {
  namespaced: true,
  state: {
    user: null,
  },
  mutations: {
    setUser(state, payload) {
      state.user = payload
    },
  },
  actions: {
    signInWithGoogle({ commit }) {
      this.dispatch('common/startLoading')
      const provider = new firebase.auth.GoogleAuthProvider()
      firebase.auth().signInWithPopup(provider)
        .then((result) => {
          commit('setUser', result.user)
          this.dispatch('common/stopLoading')
        })
        .catch((error) => {
          this.dispatch('common/showSnackbar', {color: 'error', text: error.message})
          this.dispatch('common/stopLoading')
        })
    },
    signOut({ commit }) {
      this.dispatch('common/startLoading')
      firebase.auth().signOut()
        .then(() => {
          commit('setUser', null)
          this.dispatch('common/stopLoading')
        })
        .catch((error) => {
          this.dispatch('common/showSnackbar', { color: 'error', text: error.message })
          this.dispatch('common/stopLoading')
        })
    },
    autoSignIn({ commit }, payload) {
      commit('setUser', payload)
      this.dispatch('common/stopLoading')
    },
  },
  getters: {}
}

export default moduleOfUser
