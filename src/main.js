import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'
import { auth } from './db'
import VueLazyload from 'vue-lazyload'

Vue.use(VueLazyload, {
  error: require('./assets/image-not-found.png'),
  loading: require('./assets/pre-loader.gif'),
})

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
  created() {
    store.dispatch('common/startLoading')
    auth.onAuthStateChanged((firebaseUser) => {
      if (firebaseUser) {
        store.dispatch('user/autoSignIn', firebaseUser)
      } else {
        store.dispatch('common/stopLoading')
      }
    })
  }
}).$mount('#app')
