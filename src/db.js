import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/storage'
import 'firebase/auth'

firebase.initializeApp({
  apiKey: "AIzaSyCNgGNExFao7M2B0_yUA-mtldVkTR4yjVM",
  authDomain: "nextmeal-66666.firebaseapp.com",
  databaseURL: "https://nextmeal-66666.firebaseio.com",
  projectId: "nextmeal-66666",
  storageBucket: "nextmeal-66666.appspot.com",
  messagingSenderId: "458001170065"
})

export const db = firebase.firestore()
export const storage = firebase.storage();
export const auth = new firebase.auth;